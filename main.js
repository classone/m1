//console.log('testing...')
// See https://electronjs.org/docs/tutorial/first-app

const { app, BrowserWindow, Menu } = require('electron')

// main process
const path = require('path')
const url = require('url')

let win

function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600 });
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file',
        slashes: true
    }))

    win.on('closed', () => {
        win = null
    })

    win.openDevTools()
}

app.on('ready', createWindow)

app.on('windows-all-closed', () => {
    // On Mac, leave application and menu bar active
    if (procss.platform !== 'darwin') {
        app.quite()
    }
})

app.on('activate', () => {
    // On Mac, re-create window when dock icon is clicked and no window is open
    if (win == null) {
        createWindow()
    }
})